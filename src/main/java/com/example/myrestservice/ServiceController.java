package com.example.myrestservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sap.service.contract.ServiceContract;
import com.sap.service.model.Service;


public class ServiceController implements ServiceContract {
	@Autowired
	ServiceRepository serviceRepository;


	public Long create(Service service) {
		if(service != null){
			 serviceRepository.save(service);
			return service.getId();
		}
		return -1l;
	}


	public Service read(long id) {
		Service service;
		if( id > 0){
			service = serviceRepository.findOne(id);
			return service;
		}
		return null;
	};

	
	public List<Service> readAll() {
		List<Service>services;
		services = (List<Service>) serviceRepository.findAll();
		return services;
	};

	public Long update(Service service) {
		Long id ;
		if(service != null){
			id = create(service);
			return id;
		}
		return -1l;
	}


	public Service delete(long id) {
		Service service;
		if(serviceRepository.exists(id)){
			service = read(id);
			serviceRepository.delete(id);
			return service;
		}
		return null;
	};


	public String echo(String echo) {
		return echo;
	}
}
