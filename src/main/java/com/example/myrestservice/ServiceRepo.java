package com.example.myrestservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.service.model.Service;

public final class ServiceRepo {

	private static final ServiceRepo INSTANCE = new ServiceRepo();
	private Map<Long, Service> services;
	private Long count;

	private ServiceRepo() {
		if (services == null) {
			services = new HashMap<Long, Service>();
			count = 0l;
		}
		Service service = new Service("name", "description", "note", 0.0, 0.0, 0.0, 0.0);
		create(service);
	}

	public Long create(Service service) {
		if (service.getId() == null) {
			count++;
			service.setId(count);
			services.put(count, service);
			return service.getId();
		} else
			return -1L;
	}

	public Service read(Long id) {
		if (services.containsKey(id)) {
			System.out.println("id exists");
			return services.get(id);
		} else
			return null;
	}

	public List<Service> readAll() {
		List<Service> serviceList = new ArrayList<Service>(services.values());
		return serviceList;
	}

	public Long update(Service service) {
		Long id = service.getId();
		if (services.containsKey(id)) {
			services.put(id, service);
			return id;
		} else
			return -1L;
	}

	public Service delete(Long id) {
		if (services.containsKey(id)) {
			Service service = services.get(id);
			services.remove(id);
			return service;
		} else
			return null;
	}

	public static final ServiceRepo getInstance() {
		return INSTANCE;
	}
}
