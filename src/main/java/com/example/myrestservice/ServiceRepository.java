/**
 * 
 */
package com.example.myrestservice;

import org.springframework.data.repository.CrudRepository;

import com.sap.service.model.Service;

/**
 * @author I303450
 *
 */
public interface ServiceRepository extends CrudRepository<Service,Long>{

}
